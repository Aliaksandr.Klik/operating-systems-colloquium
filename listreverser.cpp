#ifndef LISTREVERSER_CPP
#define LISTREVERSER_CPP

#include "listreverser.h"

#include <list>

template <class T>
ListReverser<T>::ListReverser() = default;

template <class T>
ListReverser<T>::~ListReverser() = default;

template <class T>
std::list<T> ListReverser<T>::reverse(std::list<T> &list) {
    if (list.size() <= 1) {
        return list;
    }
    T first = list.front();
    list.pop_front();
    std::list<T> result = reverse(list);
    result.push_back(first);
    return result;
}

#endif