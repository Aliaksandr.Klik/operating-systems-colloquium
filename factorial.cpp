#include "factorial.h"

Factorial::Factorial() {
    _factorials.push_back(1);
}

Factorial::~Factorial() = default;

u_int64_t Factorial::factorial(int n) {
    if (n < 0) {
        throw std::invalid_argument("n must be >= 0");
    }
    if (n < _factorials.size()) {
        return _factorials[n];
    }
    u_int64_t result = _factorials.back();
    for (size_t i = _factorials.size(); i <= n; i++) {
        result *= i;
        _factorials.push_back(result);
    }
    return result;
}

std::vector<u_int64_t> Factorial::factorials(int n) {
    if (n < 0) {
        throw std::invalid_argument("n must be >= 0");
    }
    if (n < _factorials.size()) {
        return {_factorials.begin(), _factorials.begin() + n + 1};
    }
    u_int64_t result = _factorials.back();
    for (size_t i = _factorials.size(); i <= n; i++) {
        result *= i;
        _factorials.push_back(result);
    }
    return _factorials;
}