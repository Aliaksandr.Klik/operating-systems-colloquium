#ifndef LISTREVERSER_H
#define LISTREVERSER_H

#include <list>

template <class T>
class ListReverser {
public:
    ListReverser();
    ~ListReverser();
    static std::list<T> reverse(std::list<T>&);
};

#include "listreverser.cpp"

#endif
