#ifndef DEDUP_H
#define DEDUP_H

#import <vector>


template <class T>
class Dedup {
public:
    Dedup();
    ~Dedup();
    static std::vector<T> dedup(std::vector<T>&);
};

#include "dedup.cpp"

#endif
