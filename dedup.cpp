#ifndef DEDUP_CPP
#define DEDUP_CPP

#include "dedup.h"

#include <set>

template <class T>
Dedup<T>::Dedup() = default;

template <class T>
Dedup<T>::~Dedup() = default;

template <class T>
std::vector<T> Dedup<T>::dedup(std::vector<T> &vec) {
    std::set<T> set;
    std::vector<T> result;
    for (auto &item : vec) {
        if (set.find(item) == set.end()) {
            set.insert(item);
            result.push_back(item);
        }
    }
    return result;
}

#endif