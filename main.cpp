#include <iostream>
#include <list>
#include <vector>

#include "factorial.h"
#include "dedup.h"
#include "listreverser.h"

int main() {
    Factorial factorial;
    std::vector<u_int64_t> factorials = factorial.factorials(10);
    std::vector<u_int64_t> expected = {1, 1, 2, 6, 24, 120, 720, 5040, 40320,
                                       362880, 3628800};
    std::cout << "Number\tExpected\tActual" << std::endl;
    for (size_t i = 0; i < factorials.size(); i++) {
        std::cout << i << "\t\t" << expected[i] << "\t\t" << factorials[i] << std::endl;
    }

    std::vector<int> vec = {1, 7, 7, 2, 3, 4, 6, 5, 1, 2, 3, 4, 5};
    std::vector<int> deduped = Dedup<int>::dedup(vec);
    std::vector<int> expected2 = {1, 7, 2, 3, 4, 6, 5};
    std::cout << "Expected\tActual" << std::endl;
    for (size_t i = 0; i < deduped.size(); i++) {
        std::cout << expected2[i] << "\t\t" << deduped[i] << std::endl;
    }

    std::list<int> list = {1, 2, 3, 4, 5};
    ListReverser<int> reverser;
    std::list<int> reversed = ListReverser<int>::reverse(list);
    std::list<int> expected3 = {5, 4, 3, 2, 1};
    std::cout << "Expected\tActual" << std::endl;
    for (int & it : reversed) {
        std::cout << it << "\t\t" << expected3.front() << std::endl;
        expected3.pop_front();
    }

    return 0;
}