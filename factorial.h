#ifndef FACTORIAL_H
#define FACTORIAL_H

#include <vector>


class Factorial {
public:
    Factorial();
    ~Factorial();
    u_int64_t factorial(int n);
    std::vector<u_int64_t > factorials(int n);
private:
    std::vector<u_int64_t> _factorials;
};

#endif
